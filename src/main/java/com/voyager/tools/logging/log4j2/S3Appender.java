package com.voyager.tools.logging.log4j2;

import java.io.Serializable;
import java.util.Objects;

import org.apache.logging.log4j.core.Filter;
import org.apache.logging.log4j.core.Layout;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.appender.AbstractAppender;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.config.plugins.PluginBuilderFactory;

/**
 * The composite appender used to route log messages to S3.
 */
@Plugin(name = "S3Appender", category = "Core", elementType = "appender")
public class S3Appender extends AbstractAppender implements Flushable {

	private LoggingEventCache<Event> eventCache = null;

	@PluginBuilderFactory
	public static org.apache.logging.log4j.core.util.Builder<S3Appender> newBuilder() {
		return new S3AppenderBuilder();
	}

	public S3Appender(String name, Filter filter, Layout<? extends Serializable> layout, boolean ignoreExceptions,
			LoggingEventCache<Event> eventCache) {
		super(name, filter, layout, ignoreExceptions);
		LOGGER.info("Initializing appender: " + name);
		Objects.requireNonNull(eventCache);
		this.eventCache = eventCache;
	}

	public void append(LogEvent logEvent) {
		try {
			eventCache.add(mapToEvent(logEvent));
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		LOGGER.debug(String.format("S3Appender says: %s", logEvent.getMessage().getFormattedMessage()));
	}

	private Event mapToEvent(LogEvent event) {
		String message = null;
		if (null != getLayout()) {
			message = getLayout().toSerializable(event).toString();
		} else {
			message = event.getMessage().toString();
		}
		Event mapped = new Event(event.getLoggerName(), event.getLevel().toString(), message);
		return mapped;
	}
	
	public LoggingEventCache<Event> getEventCache() {
		return eventCache;
	}

	@Override
	public void flushEvents() {
		LOGGER.info("Publishing staging log on shutdown...");
		eventCache.shutdown();
	}
}
