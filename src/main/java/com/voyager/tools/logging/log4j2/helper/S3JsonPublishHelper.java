package com.voyager.tools.logging.log4j2.helper;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.Objects;
import java.util.zip.GZIPOutputStream;

import org.apache.http.entity.ContentType;

import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.voyager.tools.logging.log4j2.Event;
import com.voyager.tools.logging.log4j2.PublishContext;

/**
 * Implementation to publish log events to S3.
 * <br>
 * These Log4j logger parameters configure the S3 publisher:
 * <br>
 * <em>NOTES</em>:
 * <ul>
 * <li>If the access key and secret key are provided, they will be preferred over
 * whatever default setting (e.g. ~/.aws/credentials or
 * %USERPROFILE%\.aws\credentials) is in place for the
 * runtime environment.</li>
 * <li>Tags are currently ignored by the S3 publisher.</li>
 * </ul>
 *
 * @author vly
 * @author whiteskylabs
 *
 */
public class S3JsonPublishHelper implements PublishHelper<Event> {
    private static final String LINE_SEPARATOR_CONST = System.getProperty("line.separator");
    private static final String COMMA_CONST = ",";
    private static final String OPEN_BRACKET_CONST = "[";
    private static final String CLOSED_BRACKET_CONST = "]";

    private final AmazonS3Client client;
    private final String bucket;
    private final String path;
    private boolean compressEnabled = false;

    private volatile boolean bucketExists = false;

    private File tempFile;
    private Writer outputWriter;


    public S3JsonPublishHelper(AmazonS3Client client, String bucket, String path, boolean compressEnabled) {
        this.client = client;
        this.bucket = bucket.toLowerCase();
        if (!path.endsWith("/")) {
            this.path = path + "/";
        } else {
            this.path = path;
        }
        this.compressEnabled = compressEnabled;
    }

    public void start(PublishContext context) {
        try {
            tempFile = File.createTempFile("s3Publish", null);
            OutputStream os = createCompressedStreamAsNecessary(
                new BufferedOutputStream(new FileOutputStream(tempFile)),
                compressEnabled);
            outputWriter = new OutputStreamWriter(os);
            outputWriter.write(OPEN_BRACKET_CONST);
            outputWriter.write(LINE_SEPARATOR_CONST);

            if (!bucketExists) {
                bucketExists = client.doesBucketExist(bucket);
                if (!bucketExists) {
                    client.createBucket(bucket);
                    bucketExists = true;
                }
            }
        } catch (Exception ex) {
            throw new RuntimeException(String.format("Cannot start publishing: %s", ex.getMessage()), ex);
        }
    }

    public void publish(PublishContext context, int current, int count, Event event) {
        try {
        	if (current < (count-1))
        		outputWriter.write(event.getMessage() + COMMA_CONST);
        	else 
        		outputWriter.write(event.getMessage());
            outputWriter.write(LINE_SEPARATOR_CONST);
        } catch (Exception ex) {
            throw new RuntimeException(
                String.format("Cannot collect event %s: %s", event, ex.getMessage()), ex);
        }
    }


    public void end(PublishContext context) {
        String key = String.format("%s%s", path, context.getCacheName());
        try {
            if (null != outputWriter) {
            	outputWriter.write(CLOSED_BRACKET_CONST);
                outputWriter.close();
                outputWriter = null;
                ObjectMetadata metadata = new ObjectMetadata();
                metadata.setContentLength(tempFile.length());
                metadata.setContentType(ContentType.DEFAULT_BINARY.getMimeType());

                PutObjectRequest por = new PutObjectRequest(bucket, key, tempFile);
                por.setMetadata(metadata);
                
                client.putObject(por);
            }
        } catch (UnsupportedEncodingException e) {
        } catch (Exception ex) {
            throw new RuntimeException(
                String.format("Cannot publish to S3: %s", ex.getMessage()), ex);
        } finally {
            if (null != tempFile) {
                try {
                    tempFile.delete();
                    tempFile = null;
                } catch (Exception ex) {
                }
            }
        }
    }

    static OutputStream createCompressedStreamAsNecessary(
        OutputStream outputStream, boolean compressEnabled) throws IOException {
        Objects.requireNonNull(outputStream);
        if (compressEnabled) {
            return new GZIPOutputStream(outputStream);
        } else {
            return outputStream;
        }
    }
}
