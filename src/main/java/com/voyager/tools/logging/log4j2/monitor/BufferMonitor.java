package com.voyager.tools.logging.log4j2.monitor;

import com.voyager.tools.logging.log4j2.FlushAndPublish;

/**
 * Monitors a {@link FlushAndPublish} and flushes the buffer depending on the
 * implemented policy. The {@link FlushAndPublish#flushAndPublish()} can be used
 * by implementations to flush the queue.
 */
public interface BufferMonitor<T> {
	/**
	 * A log event has just been added to the event buffer. Handlers can decide to
	 * flush the buffer if the conditions are right.
	 *
	 * @param event
	 *            the event just added to the buffer.
	 * @param flushAndPublisher
	 *            the {@link FlushAndPublish} to use to publish.
	 */
	void eventAdded(final T event, final FlushAndPublish flushAndPublisher);

	/**
	 * Method used to shutdown the Cache Monitor
	 */
	void shutdown();
}
