package com.voyager.tools.logging.log4j2.config;

public abstract class AbstractEsbAppenderConfiguration {

	private String appenderName;
	
	private boolean verbose;
	
	private int stagingBufferAge;
	
	private String serviceName = "esb";
	
	private String component;
	
	private String tags;
	
	public String getAppenderName() {
		return appenderName;
	}

	public void setAppenderName(String appenderName) {
		this.appenderName = appenderName;
	}

	public boolean isVerbose() {
		return verbose;
	}

	public void setVerbose(boolean verbose) {
		this.verbose = verbose;
	}

	public int getStagingBufferAge() {
		return stagingBufferAge;
	}

	public void setStagingBufferAge(int stagingBufferAge) {
		this.stagingBufferAge = stagingBufferAge;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public String getComponent() {
		return component;
	}

	public void setComponent(String component) {
		this.component = component;
	}
	
	public String getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}
}
