package com.voyager.tools.logging.log4j2;

public interface Flushable {

	void flushEvents();
}
