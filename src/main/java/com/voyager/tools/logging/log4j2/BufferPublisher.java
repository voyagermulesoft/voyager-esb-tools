package com.voyager.tools.logging.log4j2;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.TimeZone;

import com.voyager.tools.logging.log4j2.helper.PublishHelper;

/**
 * Implementation to standardize on a cache name and aggregate and coordinate
 * multiple IPublishHelpers to publish content to different destinations.
 *
 * @author vly
 * @author whiteskylabs
 *
 */
public class BufferPublisher<T> {
	private final String hostName;
	private final String[] tags;
	private final String serviceName;
	private final String component;
	private final String extension;

	private List<PublishHelper<T>> helpers = new LinkedList<>();

	public BufferPublisher(String hostName, String[] tags, String serviceName, String component, boolean compressed) {
		this.hostName = hostName;
		this.tags = tags;
		this.serviceName = serviceName;
		this.component = component;
		this.extension = compressed ? ".gz" : ".log";
	}

	public PublishContext startPublish() {
		String namespacedCacheName = composeNamespacedCacheName();
		PublishContext context = new PublishContext(namespacedCacheName, hostName, tags);
		for (PublishHelper<T> helper : helpers) {
			try {
				helper.start(context);
			} catch (Throwable t) {
				System.err.println(
						String.format("Cannot start publish with %s due to error: %s", helper, t.getMessage()));
			}
		}
		return context;
	}

	private String composeNamespacedCacheName() {
		SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd/HHmmss");
		df.setTimeZone(TimeZone.getTimeZone("UTC"));
		String[] dateParts = df.format(new Date()).split("/");
		return String.format("%s/%s/%s/%s%s%s/%s-%s%s%s%s", serviceName, component, dateParts[0], dateParts[0],
				dateParts[1], dateParts[2], component, dateParts[0], dateParts[1], dateParts[2], dateParts[3])
				+ extension;
	}

	public void publish(PublishContext context, int current, int count, T event) {
		for (PublishHelper<T> helper : helpers) {
			try {
				helper.publish(context, current, count, event);
			} catch (Throwable t) {
				System.err.println(String.format("Cannot publish with %s due to error: %s", helper, t.getMessage()));
			}
		}
	}

	public void endPublish(PublishContext context) {
		for (PublishHelper<T> helper : helpers) {
			try {
				helper.end(context);
			} catch (Throwable t) {
				System.err
						.println(String.format("Cannot end publish with %s due to error: %s", helper, t.getMessage()));
			}
		}
	}

	/**
	 * Add an IPublishHelper implementation to the list of helpers to invoke when
	 * publishing is performed.
	 *
	 * @param helper
	 *            helper to add to the list
	 */
	public void addHelper(PublishHelper<T> helper) {
		helpers.add(helper);
	}
}
