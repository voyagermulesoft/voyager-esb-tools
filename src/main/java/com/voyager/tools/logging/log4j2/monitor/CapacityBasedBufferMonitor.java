package com.voyager.tools.logging.log4j2.monitor;

import com.voyager.tools.logging.log4j2.FlushAndPublish;
import com.voyager.tools.logging.log4j2.LoggingEventCache;

/**
 * Implementation of {@link BufferMonitor} that flushes the cache when its
 * capacity reaches a limit.
 */
public class CapacityBasedBufferMonitor<T> implements BufferMonitor<T> {

	private final int cacheLimit;
	private final Object countGuard = new Object();
	private int count = 0;
	private FlushAndPublish cache;

	/**
	 * Creates an instance with the cache limit as provided. When hooked up to a
	 * LoggingEventCache, this object will flush the cache when the cache's event
	 * count reaches the limit.
	 *
	 * @param cacheLimit
	 *            the limit in number of event messages. When the associated
	 *            {@link LoggingEventCache}'s event cache size reaches this limit,
	 *            the event cache will be flushed and published.
	 */
	public CapacityBasedBufferMonitor(int cacheLimit) {
		this.cacheLimit = cacheLimit;
	}

	@Override
	public void eventAdded(final T event, final FlushAndPublish cache) {
		boolean flush = false;
		this.cache = cache;
		synchronized (countGuard) {
			if (++count >= cacheLimit) {
				flush = true;
				count = 0;
			}
		}
		if (flush) {
			try {
				cache.flushAndPublish();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}

	@Override
	public String toString() {
		return String.format("CapacityBasedBufferMonitor(cacheLimit: %d)", cacheLimit);
	}

	@Override
	public void shutdown() {
		cache.flushAndPublish();
	}
}
