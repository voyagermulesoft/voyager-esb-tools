package com.voyager.tools.logging.log4j2;

import java.net.UnknownHostException;
import java.util.Collections;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.appender.AbstractAppender;
import org.apache.logging.log4j.core.config.plugins.PluginBuilderAttribute;
import org.apache.logging.log4j.core.filter.AbstractFilter;
import org.apache.logging.log4j.core.util.Builder;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSCredentialsProviderChain;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.voyager.tools.logging.log4j2.config.S3Configuration;
import com.voyager.tools.logging.log4j2.helper.S3JsonPublishHelper;
import com.voyager.tools.logging.log4j2.monitor.BufferMonitor;
import com.voyager.tools.logging.log4j2.monitor.CapacityBasedBufferMonitor;
import com.voyager.tools.logging.log4j2.monitor.TimePeriodBasedBufferMonitor;

public class S3AppenderBuilder extends AbstractAppender.Builder<S3AppenderBuilder> implements Builder<S3Appender> {

	// general properties
	private String appenderName;
	
	@PluginBuilderAttribute
	private boolean verbose = false;

	@PluginBuilderAttribute
	private String tags;

	@PluginBuilderAttribute
	private int stagingBufferSize = 25;

	@PluginBuilderAttribute
	private int stagingBufferAge = 0;

	@PluginBuilderAttribute
	private String serviceName = "No Service Name";

	@PluginBuilderAttribute
	private String component = "No Component";

	// S3 properties
	@PluginBuilderAttribute
	private String s3Bucket;

	@PluginBuilderAttribute
	private String s3Region;

	@PluginBuilderAttribute
	private String s3Path;

	@PluginBuilderAttribute
	private String s3AwsKey;

	@PluginBuilderAttribute
	private String s3AwsSecret;

	@PluginBuilderAttribute
	private String s3Compression = "true";

	public S3AppenderBuilder() {		
	}
	
	public S3AppenderBuilder(S3Configuration config) {
		
		this.appenderName = config.getAppenderName();
		this.verbose = config.isVerbose();
		this.tags = config.getTags();
		this.stagingBufferAge = config.getStagingBufferAge();
		this.serviceName = config.getServiceName();
		this.component = config.getComponent();
		this.s3Bucket = config.getBucket();
		this.s3Region = config.getRegion().getName();
		this.s3Path = config.getPath();
		this.s3AwsKey = config.getAccessKey();
		this.s3AwsSecret = config.getSecretKey();
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public S3Appender build() {
		try {
			String cacheName = UUID.randomUUID().toString().replaceAll("-", "");
			String nameParam =  getName() == null ? appenderName : getName();
			LoggingEventCache<Event> cache = new LoggingEventCache<>(cacheName, createCacheMonitor(), createCachePublisher(), appenderName);
			return installFilter(new S3Appender(nameParam, getFilter(), getLayout(), true, cache));
		} catch (Exception e) {
			throw new RuntimeException("Cannot build appender due to errors", e);
		}
	}

	private S3Appender installFilter(S3Appender appender) {
		appender.addFilter(new AbstractFilter() {
			@Override
			public Result filter(final LogEvent event) {
				// To prevent infinite looping, we filter out events from
				// the publishing thread
				Result decision = Result.NEUTRAL;
				if (LoggingEventCache.PUBLISH_THREAD_NAME.equals(event.getThreadName())) {
					decision = Result.DENY;
				}
				return decision;
			}
		});
		return appender;
	}

	private Optional<AmazonS3> initS3ClientIfEnabled() {
		Optional<S3Configuration> s3 = Optional.empty();
		if ((null != s3Bucket) && (null != s3Path)) {
			S3Configuration config = new S3Configuration();
			config.setBucket(s3Bucket);
			config.setPath(s3Path);
			config.setRegion(s3Region);
			config.setAccessKey(s3AwsKey);
			config.setSecretKey(s3AwsSecret);
			s3 = Optional.of(config);
		}
		return s3.map(config -> AmazonS3ClientBuilder.standard().withCredentials(getCredentialsProvider(config.getAccessKey(), config.getSecretKey())).withRegion(config.getRegion().toString()).build());
	}

	private BufferPublisher<Event> createCachePublisher() throws UnknownHostException {

		java.net.InetAddress addr = java.net.InetAddress.getLocalHost();
		String hostName = addr.getHostName();
		boolean isCompressed = Boolean.parseBoolean(s3Compression);
		BufferPublisher<Event> publisher = new BufferPublisher<Event>(hostName, parseTags(tags), serviceName, component, isCompressed);

		initS3ClientIfEnabled().ifPresent(client -> {
			if (verbose) {
				System.out.println(String.format("Registering S3 publish helper -> %s:%s", s3Bucket, s3Path));
			}
			publisher.addHelper(new S3JsonPublishHelper((AmazonS3Client) client, s3Bucket, s3Path, isCompressed));
		});

		return publisher;
	}

	private AWSCredentialsProvider getCredentialsProvider(String key, String secret) {
		AWSCredentialsProvider credProvider;
		if ((null != key) && (null != secret)) {
			credProvider = new AWSCredentialsProviderChain(
					// If the user took the pains to construct us with the
					// access
					// credentials, give them priority over the defaults from
					// the
					// the more general environment
					new AWSCredentialsProvider() {
						public AWSCredentials getCredentials() {
							return new BasicAWSCredentials(key, secret);
						}

						public void refresh() {
						}
					}, new DefaultAWSCredentialsProviderChain());
		} else {
			credProvider = new DefaultAWSCredentialsProviderChain();
		}
		return credProvider;
	}

	private String[] parseTags(String tags) {
		Set<String> parsedTags = null;
		if (null != tags) {
			parsedTags = Stream.of(tags.split("[,;]")).filter(Objects::nonNull).map(String::trim).collect(Collectors.toSet());
		} else {
			parsedTags = Collections.emptySet();
		}
		return parsedTags.toArray(new String[] {});
	}

	private BufferMonitor<Event> createCacheMonitor() {
		BufferMonitor<Event> monitor = new CapacityBasedBufferMonitor<Event>(stagingBufferSize);
		if (0 < stagingBufferAge) {
			monitor = new TimePeriodBasedBufferMonitor<Event>(stagingBufferAge);
		}
		if (verbose) {
			System.out.println(String.format("Using cache monitor: %s", monitor.toString()));
		}
		return monitor;
	}
}
