package com.voyager.tools.mule.context.listener;

import java.util.Map;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.Appender;
import org.apache.logging.log4j.core.Filter;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.config.LoggerConfig;
import org.apache.logging.log4j.core.filter.CompositeFilter;
import org.apache.logging.log4j.core.filter.Filterable;
import org.apache.logging.log4j.core.filter.MarkerFilter;
import org.apache.logging.log4j.core.layout.PatternLayout;
import org.mule.api.context.notification.MuleContextNotificationListener;
import org.mule.context.notification.MuleContextNotification;
import org.springframework.beans.factory.annotation.Value;

import com.voyager.tools.logging.log4j2.S3Appender;
import com.voyager.tools.logging.log4j2.S3AppenderBuilder;
import com.voyager.tools.logging.log4j2.config.S3Configuration;

public class LogAppenderSetupContextListener implements MuleContextNotificationListener<MuleContextNotification> {

	private static final Logger logger = LogManager.getLogger(LogAppenderSetupContextListener.class);

	@Value("${voyager.logs.dev.flag}")
	private boolean devLog;

	@Value("${voyager.logs.audit.flag}")
	private boolean auditLog;

	private Map<String, S3Configuration> s3AppenderMap;

	@Override
	public void onNotification(MuleContextNotification notification) {

		if (notification.getAction() == MuleContextNotification.CONTEXT_STARTING) {
			LoggerContext context = (LoggerContext) LogManager.getContext();
			Map<String, Appender> appenderMap = context.getConfiguration().getAppenders();

			logger.info("Mule Context Starting...Setting up Log4j2 Appenders.");
			appenderMap.entrySet().stream().forEach(entry -> {
				addCloudhubMarkerFilters(entry.getValue());
			});

			addS3Appenders(context.getConfiguration().getRootLogger());

			logger.info("Logging to DEV is set to: " + devLog);
			logger.info("Logging to AUDIT is set to: " + auditLog);
		}
	}

	private void addS3Appenders(LoggerConfig config) {

		s3AppenderMap.entrySet().stream().forEach(entry -> {
			logger.info("Initializing appender: " + entry.getValue().getAppenderName());
			S3Appender appender = new S3AppenderBuilder(entry.getValue()).withLayout(PatternLayout.newBuilder().withPattern("%m").build()).build();
			config.addAppender(appender, Level.INFO, MarkerFilter.createFilter(appender.getName().toUpperCase().contains("DEV") ? "DEV" : "AUDIT", Filter.Result.ACCEPT, Filter.Result.DENY));
			appender.start();
		});
	}

	private void addCloudhubMarkerFilters(Appender appenderParam) {

		if (appenderParam.getClass().getSimpleName().equals("Log4J2CloudhubLogAppender") && appenderParam instanceof Filterable) {
			Filterable appender = (Filterable) appenderParam;
			Filter[] filters = { MarkerFilter.createFilter("DEV", Filter.Result.DENY, Filter.Result.NEUTRAL), MarkerFilter.createFilter("AUDIT", Filter.Result.DENY, Filter.Result.NEUTRAL) };
			appender.addFilter(CompositeFilter.createFilters(filters));
		}
	}

	public void setAppenderMap(Map<String, S3Configuration> appenderMap) {
		this.s3AppenderMap = appenderMap;
	}
}
