package com.voyager.tools.mule.context.listener;

import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.Appender;
import org.apache.logging.log4j.core.LoggerContext;
import org.mule.api.context.notification.MuleContextNotificationListener;
import org.mule.context.notification.MuleContextNotification;

import com.voyager.tools.logging.log4j2.Flushable;

public class S3PublishingContextListener implements MuleContextNotificationListener<MuleContextNotification> {

	private static final Logger logger = LogManager.getLogger(S3PublishingContextListener.class);

	@Override
	public void onNotification(MuleContextNotification notification) {

		LoggerContext context = (LoggerContext) LogManager.getContext();
		Map<String, Appender> appenderMap = context.getConfiguration().getRootLogger().getAppenders();

		if (notification.getAction() == MuleContextNotification.CONTEXT_STOPPING) {
			logger.info("Mule Context Stopping...Flushing log events.");
			appenderMap.entrySet().stream().forEach(entry -> {
				if (entry.getValue() instanceof Flushable)
					((Flushable) entry.getValue()).flushEvents();
			});
			logger.info("Mule Context Stopping...Flushing complete.");
		}
	}
}
